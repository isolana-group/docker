#!/bin/bash

usage() { echo "Usage: $0 --genesis genesis --ports ports --rpc rpc --entrypoint_host entrypoint_host --entrypoint_port entrypoint_port" 1>&2; exit 1; }

if [ "$#" !=  "10" ]
    then
         usage
fi

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --genesis) genesis="$2"; shift ;;
        --ports) ports="$2"; shift ;;
        --rpc) rpc="$2"; shift ;;
        --entrypoint_host) entrypoint_host="$2"; shift ;;
        --entrypoint_port) entrypoint_port="$2"; shift ;;
        *) usage ;;
    esac
    shift
done

while ! nc -z $entrypoint_host $entrypoint_port; do
  sleep 0.1 # wait for 1/10 of the second before check again
done

solana config set --url http://$entrypoint_host:8899
solana-keygen new -o /solana/id.json --no-bip39-passphrase
solana-keygen new -o /solana/id_vote.json --no-bip39-passphrase

solana-validator \
            --identity /solana/id.json \
            --vote-account /solana/id_vote.json \
            --ledger ledger \
            --rpc-port $rpc \
            --entrypoint $entrypoint_host:$entrypoint_port \
            --dynamic-port-range $ports \
            --expected-genesis-hash $genesis \
            --expected-bank-hash $genesis \
            --wal-recovery-mode skip_any_corrupted_record \
            --known-validator 8eXrEgkfacuTfUyuDzqwR8zwPJ5kUNKz5xHop3NVYwR5 \
            --no-snapshot-fetch \
            --halt-on-known-validators-accounts-hash-mismatch \
            --only-known-rpc \
            --log - \
            run
