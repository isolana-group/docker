# Docker.
## Requirements.

Linux. 

Docker 20.10.2+. 

Docker compose 1.25.0+.

## Install soft.
```
    sudo apt update && apt upgrade
    sudo apt install docker docker.io containerd docker-compose git zip htop
```

## Get docker.
```
    mkdir -p /var/docker/isolana-group && cd /var/docker/isolana-group
    git clone git@gitlab.com:isolana-group/docker.git
```

##	Build.
```
    sudo docker-compose -f build.yml build
```

## Initialize.
```
    sudo docker-compose -f compose-solana.yml run solana_masternode bash
    
    solana-keygen new --outfile /solana/.config/id.json --word-count=24 --no-bip39-passphrase
    solana-keygen new --outfile /solana/.config/id_vote.json --word-count=24 --no-bip39-passphrase
    solana-keygen new --outfile /solana/.config/id_stake.json --word-count=24 --no-bip39-passphrase
    
    solana-genesis --bootstrap-validator Bzo812SM5hrtzh4ZjpU2KkrBtvZxosQEq3DL1VVfJHgU 5zAVPJmCpoCsKDptVHpyX2x1VFFPodTgxRCL1UXRAgDs 59WWZsggS5tVR3jSAZSZe17EpZy9Ty8utHwRBurEbUxm --faucet-pubkey Bzo812SM5hrtzh4ZjpU2KkrBtvZxosQEq3DL1VVfJHgU --faucet-lamports 20000000000000000 --ledger ledger

    #then set some params to compose.
```

## Initialize node.
```
    sudo docker-compose -f compose-solana-node.yml run solana_node bash
```

## Push.
```
    docker login -u tradingsecret
    docker push tradingsecret/solana_id
```

## Run in compose.
```
    sudo docker-compose -f compose-solana.yml -f compose-web.yml up
```

## Run in stack services.
```
    sudo docker stack deploy -c compose-solana.yml solana
    sudo docker stack deploy -c compose-web.yml solana_web
```


