FROM solana_base_frontend

COPY git/solana/explorer .

RUN npm install && npm run build
