FROM solana_base_frontend

COPY git/spl-token-wallet .

RUN yarn install && yarn build
